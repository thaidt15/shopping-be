package com.thaidt.be.controller;

import com.thaidt.be.model.dto.Request.ChangePassRequestDTO;
import com.thaidt.be.model.dto.Request.RegistrationRequestDTO;
import com.thaidt.be.payload.AuthResponse;
import com.thaidt.be.payload.LoginRequest;
import com.thaidt.be.security.TokenProvider;
import com.thaidt.be.service.EmailService;
import com.thaidt.be.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.Map;

import static com.thaidt.be.utils.Constraints.BASE_FE;

@RestController
@RequestMapping("/api/common")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        String email = loginRequest.getEmail();
        if (!userService.isEmailExist(email)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body("Tài khoản hoặc mật khẩu bạn nhập chưa đúng");
        }
        if (userService.isUserBlocked(email)){
            return  ResponseEntity.status(HttpStatus.LOCKED)
                    .body("Xin lỗi, Tài khoản của bạn đã bị chặn!");
        }
        if (!userService.isEmailVerified(email)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Vui lòng xấc nhận tài khoản của bạn trước khi đăng nhập");
        }
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        email,
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = tokenProvider.createToken(authentication);
        String role = userService.getUserRoleByEmail(email);
        return ResponseEntity.ok(new AuthResponse(token, role));
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody RegistrationRequestDTO registrationRequest) {
        try {
            userService.registerUser(registrationRequest);

            // Generate verification token
            String verificationToken = emailService.generateVerificationToken();

            // After registering the user, send a verification email
            emailService.sendVerificationEmail(registrationRequest.getEmail(), verificationToken);

            // Authenticate the user to generate the access token
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            registrationRequest.getEmail(),
                            registrationRequest.getPassword()
                    )
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            return ResponseEntity.ok("Đăng ký thành công!. Vui lòng kiểm tra email của bạn để xác nhận tài khoản của bạn.");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/verify")
    public ResponseEntity<?> verifyUser(@RequestParam String email, @RequestParam String token) {
        try {
            // Validate the token and perform user verification
            boolean verificationResult = userService.verifyUser(email, token);

            if (verificationResult) {
                return ResponseEntity.ok("Xác nhận thành công. click vào <a href=\"" + BASE_FE + "\">đây</a> để đăng nhập.");
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                        .body("Xác nhận không thành công. Liên kết xác nhận không hợp lệ.");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/forgot-password")
    public ResponseEntity<?> forgotPassword(@RequestParam String email) {
        try {

            String resetCode = userService.generateResetCode(email);

            emailService.sendResetCodeEmail(email, resetCode);

            return ResponseEntity.ok("Mã xác nhận đã được gửi đến email của bạn. Vui lòng kiểm tra email để tiếp tục.");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }



    @PostMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody ChangePassRequestDTO dto) {
        System.out.println(dto.getEmail() +" " +dto.getCode() +" "+ dto.getNewPassword());
        try {
            // Verify the reset code
            boolean codeVerificationResult = userService.verifyResetCode(dto.getEmail(), dto.getCode());

            if (codeVerificationResult) {
                // Reset the user's password
                userService.resetPassword(dto.getEmail(), dto.getNewPassword());

                return ResponseEntity.ok("Đặt lại mật khẩu thành công.");
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN)
                        .body("Mã xác nhận không đúng hoặc đã hết hạn.");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
