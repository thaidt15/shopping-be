package com.thaidt.be.controller;

import com.thaidt.be.model.dto.Respone.UserProfileResponseDTO;
import com.thaidt.be.model.entity.User;
import com.thaidt.be.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")

@CrossOrigin(origins = "http://localhost:3000")
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("/me")
    public UserProfileResponseDTO getCurrentUser() {
        return userService.getCurrentUser();
    }

}
