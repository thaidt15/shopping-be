package com.thaidt.be.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MD5Example {

    public static void main(String[] args)  {
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//        String password = "123";
//        String hashedPassword = encoder.encode(password);
//        System.out.println("BCrypt Hash: " + hashedPassword);


                String text = "<table><tr><td><img src=\"https://online.acb.com.vn/news/images/logo.jpg\" /><br><br></td></tr><tr><td>Kính gửi Quý khách hàng.</td></tr><tr><td>ACB trân trọng thông báo tài khoản <b>12690291</b> của Quý khách đã thay đổi số dư như sau:</td></tr><tr><td>Số dư mới của tài khoản trên là: <b>10,000.00 VND</b> tính đến <b>06/01/2024</b>.<br>Giao dịch mới nhất:Ghi có <b>+5,000.00 VND</b>.<br>Nội dung giao dịch: <b>2000 GD 207638-010623 23:54:10</b>.<br><br>Cảm ơn Quý khách hàng đã sử dụng Sản phẩm/ Dịch vụ của ACB.</td></tr><tr><td>Chúng tôi mong được tiếp tục phục vụ Quý khách hàng.</td></tr><tr><td>Trân trọng.</td></tr>";

                // Extract amount
        Pattern pattern = Pattern.compile("Ghi có <b>([^<]+)</b>");
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String amount = matcher.group(1).replaceAll("\\D","");
            System.out.println(amount);
            int a = (Integer.parseInt(amount));
            System.out.println(a/100);
        }

        // Extract user id
        pattern = Pattern.compile("(\\d+) GD");
        matcher = pattern.matcher(text);
        if (matcher.find()) {
            System.out.println(matcher.group(1));
        }

        // Extract date
         pattern = Pattern.compile("(\\d{6} \\d{2}:\\d{2}:\\d{2})");
         matcher = pattern.matcher(text);
        if (matcher.find()) {
            System.out.println(matcher.group(1));
        }

    }
}