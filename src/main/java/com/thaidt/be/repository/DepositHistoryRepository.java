package com.thaidt.be.repository;

import com.thaidt.be.model.entity.DepositHistory;
import com.thaidt.be.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface DepositHistoryRepository extends JpaRepository<DepositHistory,Long> {
    DepositHistory findByUser_IdAndAmountAndDepositHistoryDate(Long userId, Double amount, Date date);
}
