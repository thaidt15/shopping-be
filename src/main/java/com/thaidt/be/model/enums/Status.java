package com.thaidt.be.model.enums;

public enum Status {
    ACTIVE,
    INACTIVE
}
