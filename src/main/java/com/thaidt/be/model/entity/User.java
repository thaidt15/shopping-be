package com.thaidt.be.model.entity;

import javax.persistence.*;

import com.thaidt.be.model.enums.Status;
import lombok.*;

import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="user_seq")
    @SequenceGenerator(name="user_seq", sequenceName="user_seq", initialValue=2000)
    private Long id;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "role", nullable = false)
    private String role;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "account_balance")
    private double accountBalance;

    @Column(name = "reset_code")
    private String resetCode;

    @Column(name = "is_verify", nullable = false, columnDefinition = "boolean default false")
    private boolean isVerify;

    @Column(name = "verification_token")
    private String verificationToken;

    @Column(name = "purchase_history")
    @OneToMany(mappedBy = "user")
    private List<Purchase> purchaseHistory;
}
