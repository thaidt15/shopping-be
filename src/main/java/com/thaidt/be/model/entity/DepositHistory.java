package com.thaidt.be.model.entity;

import javax.persistence.*;

import lombok.*;

import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "deposit_history")
public class DepositHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "deposit_history_date", nullable = false)
    private Date depositHistoryDate;

    @Column(name = "amount", nullable = false)
    private double amount;

}
