package com.thaidt.be.model.dto.Request;

import com.thaidt.be.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class DepositHistoryRequestDTO {
        private Long id;
        private User user;
        private Date depositHistoryDate;
        private double amount;
    }

