package com.thaidt.be.model.dto.Respone;

import com.thaidt.be.model.entity.DepositHistory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class UserProfileResponseDTO {
    private Long id;
    private String email;
    private String fullName;
    private String accountBalance;
}
