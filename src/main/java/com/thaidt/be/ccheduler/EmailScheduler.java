//package com.thaidt.be.ccheduler;
//
//import com.thaidt.be.service.EmailService;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import javax.mail.MessagingException;
//import java.io.IOException;
//
//@Component
//public class EmailScheduler {
//
//    private final EmailService gmailService;
//
//    public EmailScheduler(EmailService gmailService) {
//        this.gmailService = gmailService;
//    }
//
//    @Scheduled(fixedRate = 6000) // Adjust the rate as needed (in milliseconds)
//    public void readEmails() {
//        try {
//            gmailService.readGmailMessages();
//        } catch (MessagingException | IOException e) {
//            e.printStackTrace();
//        }
//    }
//}
