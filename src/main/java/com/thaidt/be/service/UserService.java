package com.thaidt.be.service;

import com.thaidt.be.model.dto.Request.RegistrationRequestDTO;
import com.thaidt.be.model.dto.Respone.UserProfileResponseDTO;
import com.thaidt.be.model.entity.User;

public interface UserService {
    void registerUser(RegistrationRequestDTO registrationRequest);
    String getUserRoleByEmail(String email);
    boolean isEmailVerified(String email);
    boolean isUserBlocked(String email);
    boolean verifyUser(String email, String token);
    boolean isEmailExist(String email);
    String generateResetCode(String email);
    boolean verifyResetCode(String email, String resetCode);
    void resetPassword(String email, String newPassword);
    UserProfileResponseDTO getCurrentUser();
}
