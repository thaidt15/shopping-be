package com.thaidt.be.service;

import com.thaidt.be.model.entity.DepositHistory;

public interface DepositHistoryService {
    void  saveDepositHistory(DepositHistory depositHistory);
}
