package com.thaidt.be.service.impl;
import com.thaidt.be.model.dto.Request.RegistrationRequestDTO;
import com.thaidt.be.model.dto.Respone.UserProfileResponseDTO;
import com.thaidt.be.model.entity.User;
import com.thaidt.be.model.enums.Status;
import com.thaidt.be.repository.UserRepository;
import com.thaidt.be.security.UserPrincipal;
import com.thaidt.be.service.EmailService;
import com.thaidt.be.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Date;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private EmailService emailService;

    @Override
    public void registerUser(RegistrationRequestDTO registrationRequest) {
        Optional<User> existingUser = userRepository.findByEmail(registrationRequest.getEmail());
        if (existingUser.isPresent()) {
            throw new IllegalArgumentException(registrationRequest.getEmail() + " Đã tồn tại! Hãy bấm quên mật khẩu nếu bạn không nhớ mật khẩu của mình!");
        }
        User user = modelMapper.map(registrationRequest, User.class);
        user.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));
        user.setRole("USER");
        user.setStatus(Status.ACTIVE);
        user.setCreatedDate(new Date());
        user.setAccountBalance(0);
        userRepository.save(user);
    }

    @Override
    public String getUserRoleByEmail(String email) {
        Optional<User> userOptional = userRepository.findByEmail(email);
        return userOptional.map(User::getRole).orElse(null);
    }

    @Override
    public boolean isEmailVerified(String email) {
        Optional<User> userOptional = userRepository.findByEmail(email);
        return userOptional.map(User::isVerify).orElse(false);
    }
    @Override
    public boolean isEmailExist(String email) {
        Optional<User> userOptional = userRepository.findByEmail(email);
        if (userOptional.isPresent()){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public boolean isUserBlocked(String email) {
        User userOptional = userRepository.findUserByEmail(email);
        return userOptional.getStatus() != Status.ACTIVE;
    }
    @Override
    public boolean verifyUser(String email, String token) {
        Optional<User> optionalUser = userRepository.findByEmail(email);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (user.getVerificationToken() != null && user.getVerificationToken().equals(token)) {
                user.setVerify(true);
                user.setVerificationToken(null);
                userRepository.save(user);

                return true;
            }
        }

        return false;
    }

    @Override
    public String generateResetCode(String email) {
        // Generate a random 6-digit reset code
        SecureRandom secureRandom = new SecureRandom();
        int resetCode = secureRandom.nextInt(900000) + 100000;

        // Save the reset code in the user entity (you may want to add a new field for this)
        User user = userRepository.findUserByEmail(email);
        if (user == null){
            throw  new IllegalArgumentException("Email bạn vừa nhập không tồn tại. Vui lòng kiểm tra và thử lại");
        }
        user.setResetCode(String.valueOf(resetCode));
        userRepository.save(user);

        return String.valueOf(resetCode);
    }

    @Override
    public boolean verifyResetCode(String email, String resetCode) {
        User user = userRepository.findUserByEmail(email);
        // Check if the provided reset code matches the stored reset code
        return user != null && resetCode.equals(user.getResetCode());
    }

    @Override
    public void resetPassword(String email, String newPassword) {
        User user = userRepository.findUserByEmail(email);
        // Reset the user's password
        user.setPassword(passwordEncoder.encode(newPassword));
        user.setResetCode(null);  // Reset the reset code after successful password reset
        userRepository.save(user);
    }

    @Override
    public UserProfileResponseDTO getCurrentUser() {
        UserPrincipal currentUserPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=  userRepository.findById(currentUserPrincipal.getId())
                .orElseThrow(() -> new RuntimeException("Current User not found"));

        UserProfileResponseDTO responseDTO = modelMapper.map(user, UserProfileResponseDTO.class);
        return responseDTO;
    }

}
