package com.thaidt.be.service.impl;

import com.thaidt.be.model.entity.DepositHistory;
import com.thaidt.be.model.entity.User;
import com.thaidt.be.repository.DepositHistoryRepository;
import com.thaidt.be.repository.UserRepository;
import com.thaidt.be.service.EmailService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.thaidt.be.utils.Constraints.BASE_HOST;

@Service
public class EmailServiceImpl implements EmailService {

    @Value("${spring.mail.sender.username}")
    private String fromEmail;
    @Autowired
    UserRepository userRepository;
    @Autowired
    DepositHistoryRepository depositHistoryRepository;
    private final JavaMailSender senderMailSender;
    private final JavaMailSender readerMailSender;
    @Autowired
    private SimpMessagingTemplate messagingTemplate;
    public EmailServiceImpl(@Qualifier("senderMailSender") JavaMailSender senderMailSender,
                            @Qualifier("readerMailSender") JavaMailSender readerMailSender) {
        this.senderMailSender = senderMailSender;
        this.readerMailSender = readerMailSender;
    }


    @Override
    public void sendVerificationEmail(String toEmail, String verificationToken) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(fromEmail);
        message.setTo(toEmail);

        // Construct the token with expiration time
        long expirationMillis = System.currentTimeMillis() + 10 * 60 * 1000;
        String tokenWithExpiration = verificationToken + "|" + expirationMillis;
        String hashedToken = hashFunction(tokenWithExpiration);
        User user = userRepository.findUserByEmail(toEmail);
        user.setVerificationToken(hashedToken);
        userRepository.save(user);
        message.setSubject("Xác nhận tài khoản tại HiHi Shop");
        message.setText("Cảm ơn bạn đã đăng ký tài khoản tại HiHi Shop... Vui lòng xác nhận tài khoản bằng cách" +
                "ấn vào link : " + BASE_HOST + "/api/common/verify?email=" + toEmail + "&token=" + hashedToken + "\n" +
                "Email này sẽ có hiệu lục trong vòng 10 phút" + "\n" +
                "Cảm ơn bạn <3");
        senderMailSender.send(message);
    }

    public String generateVerificationToken() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] randomBytes = new byte[32];
        secureRandom.nextBytes(randomBytes);
        return Base64.getUrlEncoder().withoutPadding().encodeToString(randomBytes);
    }

    public String hashFunction(String input) {
        return DigestUtils.sha256Hex(input);
    }

    @Override
    public void sendResetCodeEmail(String toEmail, String resetCode) {
        Optional<User> user = userRepository.findByEmail(toEmail);
        if (user.isEmpty()) {
            throw new IllegalArgumentException("Email bạn vừa nhập không tồn tại. Vui lòng kiểm tra và thử lại");
        }
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(fromEmail);
        message.setTo(toEmail);

        message.setSubject("Mã Xác Nhận Đặt Lại Mật Khẩu tại HiHi Shop");
        message.setText("Chào bạn,\n\n"
                + "Dưới đây là mã xác nhận để đặt lại mật khẩu của bạn tại HiHi Shop:\n\n"
                + resetCode + "\n\n"
                + "Mã này sẽ có hiệu lực trong vòng 10 phút.\n\n"
                + "Cảm ơn bạn!"
        );

        senderMailSender.send(message);
    }
    @Scheduled(cron = "*/10 * * * * *") // every 10s
    public void readGmailMessages() throws MessagingException, IOException {
        Session session = Session.getDefaultInstance(new Properties());
        Store store = session.getStore("imaps");
        store.connect("imap.gmail.com", "thaidt15@gmail.com", "zmgfrxiiscfryjjv");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();


        Folder inbox = store.getFolder("inbox");
        inbox.open(Folder.READ_ONLY);

        Message[] messages = inbox.getMessages();
        for (Message message : messages) {
//            System.out.println("Start at: "+dateFormat.format(cal.getTime()));
//            System.out.println("Subject: " + message.getSubject());
            // Check if subject is not null before invoking equals
            String subject = message.getSubject();
            if (subject != null && subject.equals("ACB-Dich vu bao so du tu dong")
                    && message.getFrom()[0].toString().contains("mailalert@acb.com.vn")) {

                // Display content
                if (message.getContent() instanceof Multipart) {
                    Multipart multipart = (Multipart) message.getContent();
                    for (int i = 0; i < multipart.getCount(); i++) {
                        BodyPart bodyPart = multipart.getBodyPart(i);
                        if (bodyPart.getContent() instanceof MimeMessage) {
                            MimeMessage mimeMessage = (MimeMessage) bodyPart.getContent();
                            String emailContent = String.valueOf(mimeMessage.getContent());
                            processAndSaveEmailContent(emailContent);
//                            System.out.println("end at: "+dateFormat.format(cal.getTime()));
                        }
                    }
                }
                else {
                    String emailContent = String.valueOf(message.getContent());
                   processAndSaveEmailContent(emailContent);
//                    System.out.println("end at: "+dateFormat.format(cal.getTime()));
                }
            }
        }

        inbox.close(false);
        store.close();
    }
    private void processAndSaveEmailContent(String emailContent) {
        DepositHistory depositHistory = extractDepositHistoryFromEmail(emailContent);
        DepositHistory exist = depositHistoryRepository.findByUser_IdAndAmountAndDepositHistoryDate(
                depositHistory.getUser().getId(),
                depositHistory.getAmount(),
                depositHistory.getDepositHistoryDate()
        );

        if (exist == null){
            depositHistoryRepository.save(depositHistory);
            User user = userRepository.findUserById(depositHistory.getUser().getId());
            user.setAccountBalance(user.getAccountBalance() + depositHistory.getAmount());
            userRepository.save(user);
            messagingTemplate.convertAndSendToUser(user.getEmail(), "/queue/notifications", "Your account balance has been updated.");
        }
    }

    public DepositHistory extractDepositHistoryFromEmail(String emailContent) {
        DepositHistory depositHistory = new DepositHistory();
        Pattern pattern = Pattern.compile("Latest transaction: Credit <b>([^<]+)</b>");
        Matcher matcher = pattern.matcher(emailContent);
        String amount;
        if (matcher.find()) {
            amount = matcher.group(1).replaceAll("\\D", "");
            int a = (Integer.parseInt(amount));
            depositHistory.setAmount(Double.parseDouble(String.valueOf(a/100)));
        }

        // Extract user id
        pattern = Pattern.compile("(\\d+) GD");
        matcher = pattern.matcher(emailContent);
        if (matcher.find()) {
           User user = userRepository.findUserById(Long.valueOf(matcher.group(1)));
            depositHistory.setUser(user);
        }

        pattern = Pattern.compile("(\\d{6} \\d{2}:\\d{2}:\\d{2})");
        matcher = pattern.matcher(emailContent);
        if (matcher.find()) {
            try {
                depositHistory.setDepositHistoryDate(parseDate(matcher.group(1)));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

        }

        return depositHistory;
    }

    private Date parseDate(String s) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("MMddyy HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date date = inputFormat.parse(s);

        String formattedDate = outputFormat.format(date);

        return outputFormat.parse(formattedDate);
    }
}
