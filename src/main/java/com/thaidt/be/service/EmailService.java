package com.thaidt.be.service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public interface EmailService {
    void sendVerificationEmail(String toEmail, String verificationToken) ;
    String generateVerificationToken();
    String hashFunction(String token);
    void sendResetCodeEmail(String toEmail, String resetCode);
    void readGmailMessages() throws MessagingException, IOException;
}
