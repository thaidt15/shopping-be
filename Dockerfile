FROM openjdk:20-jdk
WORKDIR /app
COPY ./target /app/
ENTRYPOINT ["java","-jar","/app/be-0.0.1-SNAPSHOT.jar"]